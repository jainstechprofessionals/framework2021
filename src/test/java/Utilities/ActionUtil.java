package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionUtil {
	WebDriver driver;
	
	public ActionUtil(WebDriver driver){
		this.driver= driver;
	}
	

	public void mouseHover(WebElement we) {
		Actions act = new Actions(driver);
		act.moveToElement(we).build().perform();
	}
	
	
}
