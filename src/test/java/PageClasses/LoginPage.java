package PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import BaseClasses.BaseClass;

public class LoginPage extends BaseClass{
	
	By uName = By.id("uid");
	By passWord = By.id("passw");
	By loginButton = By.name("btnSubmit");
	
	By signOff = By.xpath("//font[text()='Sign Off']");
	By errorMsg = By.xpath("//span[contains(text(),'Login Failed')]");
	
	
	public LoginPage enterUserName(String userName) {	
		sendKeys(uName,userName);
		return this;
	}
	
	public LoginPage enterPassword(String pwd) {
		sendKeys(passWord,pwd);
		return this;
	}
	
	public void clickSubmit() {
		driver.findElement(loginButton).click();
	}
	
	public boolean verifyLogin() {
		return isDisplayed(signOff);
	}
	
	public boolean verifyErrorMsg() {
		return isDisplayed(errorMsg);
	}
}
