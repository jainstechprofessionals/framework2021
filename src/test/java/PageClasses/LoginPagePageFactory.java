package PageClasses;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BaseClasses.BaseClass;

public class LoginPagePageFactory extends BaseClass{
	
	@FindBy(id= "uid")
	WebElement uname;

	@FindBy(id= "passw")
	WebElement passWord;

	@FindBy(name= "btnSubmit")
	WebElement loginButton;
	
	public LoginPagePageFactory() {
		PageFactory.initElements(driver, this);
	}
	
	
	public LoginPagePageFactory enterUserName(String userName) {	
		uname.sendKeys(userName);
		return this;
	}
	
	public LoginPagePageFactory enterPassword(String pwd) {
		passWord.sendKeys(pwd);
		return this;
	}
	
	public void clickSubmit() {
		loginButton.click();
	}
}


