package TestClasses;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BaseClasses.BaseClass;
import PageClasses.LoginPage;
import PageClasses.LoginPagePageFactory;

public class LoginTest2 extends BaseClass{
	
	LoginPage lp;
	
	@BeforeMethod
	public void setUp() {
		intialize();
		lp = new LoginPage();
		
	}
	@Test (description = "This test case is to verify the login function with valid credentials")
	public void verifyValidLogin() {
	 
	String userName = prop.getProperty("username");
	String pwd = prop.getProperty("password");
	lp.enterUserName(userName).enterPassword(pwd).clickSubmit();
	
	//Assert.assertTrue(lp.verifyLogin(), "Test case failed : As user is not able to see sign off button");
		
	}
	@Test (description = "This test case is to verify the login function with invalid credentials")
	public void verifyInValidLogin() {
	 
	
	lp.enterUserName("userName").enterPassword("pwd").clickSubmit();
	
	//Assert.assertTrue(lp.verifyErrorMsg(), "Test case failed : As user is not able to see error message");
		
	}
	
	@AfterMethod
	public void tearDown() {
		killSession();
	}

}
