package BaseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Utilities.ActionUtil;

public class BaseClass {

	public static WebDriver driver;
	public static Properties prop;
	public static ActionUtil actUtil;

	public BaseClass() {
		try {
			File f = new File(System.getProperty("user.dir") + "\\Config.properties");
			FileInputStream fis = new FileInputStream(f);
			prop = new Properties();
			prop.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void intialize() {
		String browserName = prop.getProperty("browser");
		System.out.println("User is going to login on :" + browserName);
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");

			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			// to do
		}
		String url = prop.getProperty("url");
		driver.get(url);
		System.out.println("User entering the URL : " + url);
		driver.manage().window().maximize();		
		actUtil = new ActionUtil(driver);
	}

	public void killSession() {
		driver.quit();
	}

	public WebElement getElement(By by) {
		return driver.findElement(by);
	}

	public void sendKeys(By by, String str) {
		getElement(by).sendKeys(str);
	}

	public boolean isDisplayed(By by) {
		boolean flag;
		try {
			flag = getElement(by).isDisplayed();
			return flag;
		} catch (Exception e) {
			return false;
		}

	}

}
